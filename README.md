# Vanilla++ (WIP)
A Minecraft server plugin that expands and enhances classic vanilla Minecraft. This plugin is intended to retain the vanilla feel of Minecraft while adding some "new" content and more customization of your server.

Features include...
- More mob types, like
	- Bandits,
	- Brood Mothers,
	- Player Ghosts,
	- Giant Slimes,
	- Hellhounds,
	- Wraiths,
	- and more!
	- **All mobs have configuration files so you can edit them however you like**
	- *and* you can even add completely new custom mobs by creating your own configuration
- More player options
	- Increase/decrease player's max health
	- Give players permanent [potion effects](https://minecraft.gamepedia.com/Status_effect)
- Higher difficulty
	- Change to [Hard](https://minecraft.gamepedia.com/Difficulty#Hard) difficulty on all worlds
	- Force high [regional difficulty](https://minecraft.gamepedia.com/Difficulty#Regional_difficulty) to make new chunks dangerous by default
	- Ultra Hardcore options; disable natural healing, healing from potions/food, healing from beacons, or healing from golden apples
- Customizable!
	- Enable/disable **any** of the features above as you see fit
	- All mobs can be enabled/disabled
	- Some added mobs have potion effects that could ruin regular mob farms, don't worry, you can disable those, too :)
	- Example configs will be provided for different types of servers
	
## Installation
To be filled.

## Usage
To be filled.
