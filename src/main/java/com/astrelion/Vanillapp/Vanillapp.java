package com.astrelion.Vanillapp;

import com.astrelion.Vanillapp.controllers.*;
import com.astrelion.Vanillapp.util.Util;
import org.bukkit.Chunk;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

public class Vanillapp extends JavaPlugin
{
    protected SpawnController spawnController;
    protected CommandController commandController;
    protected EventController eventController;
    protected ConfigController configController;

    @Override
    public void onEnable()
    {
        configController = new ConfigController(this);
        commandController = new CommandController(this);
        spawnController = new SpawnController(this);
        eventController = new EventController(this);
        // some controllers rely on eachother, register essential parts first
        configController.registerSelf();
        commandController.registerSelf();
        spawnController.registerSelf();
        eventController.registerSelf();
        // register children of controllers last
        configController.register();
        commandController.register();
        spawnController.register();
        eventController.register();

        for (World world : getServer().getWorlds())
        {
            getLogger().info("Setting world '" + world.getName() + "' difficulty to HARD...");
            world.setDifficulty(Difficulty.HARD);
            for (Chunk chunk : world.getLoadedChunks())
            {
                // 100 hours of IRL time
                chunk.setInhabitedTime(Util.TICKS_PER_HOUR * 100L);
            }
        }

        getLogger().info("Vanilla++ successfully enabled.");
    }

    @Override
    public void onDisable()
    {
        getLogger().info("Vanilla++ successfully disabled.");
    }

    public SpawnController getSpawnController()
    {
        return this.spawnController;
    }

    public CommandController getCommandController()
    {
        return this.commandController;
    }

    public EventController getEventController()
    {
        return this.eventController;
    }

    public ConfigController getConfigController()
    {
        return this.configController;
    }
}
