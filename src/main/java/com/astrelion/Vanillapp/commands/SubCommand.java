package com.astrelion.Vanillapp.commands;

import com.astrelion.Vanillapp.controllers.ARegisterable;
import com.astrelion.Vanillapp.controllers.CommandController;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.HashMap;
import java.util.Map;

public abstract class SubCommand extends ARegisterable
{
    protected CommandController vppCommand;
    protected Object parent;
    protected CommandSender sender;
    protected Map<String, SubCommand> subCommands = new HashMap<>();

    public SubCommand(CommandController vppCommand)
    {
        this.vppCommand = vppCommand;
        this.parent = vppCommand;
    }

    public SubCommand(CommandController vppCommand, SubCommand parent)
    {
        this.vppCommand = vppCommand;
        this.parent = parent;
    }

    public Map<String, SubCommand> getSubCommands()
    {
        return this.subCommands;
    }

    public CommandSender getCommandSender()
    {
        return sender;
    }

    public Object getParentCommand()
    {
        return this.parent;
    }

    public abstract boolean onCommand(CommandSender sender, Command command, String[] args);
}
