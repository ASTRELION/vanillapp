package com.astrelion.Vanillapp.commands;

import com.astrelion.Vanillapp.controllers.CommandController;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class VanillappTabCompleter implements TabCompleter
{
    protected CommandController vppCommand;

    public VanillappTabCompleter(CommandController vppCommand)
    {
        this.vppCommand = vppCommand;
    }

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args)
    {
        if (sender instanceof Player)
        {
            Map<String, SubCommand> subCommands = vppCommand.getSubCommands();

            // leaf SubCommands have null subCommands
            if (subCommands != null)
            {
                List<String> suggestions = new ArrayList<>();

                // go through each argument provided, eg. /vpp arg1 arg2 arg3
                for (int i = 0; i < args.length; i++)
                {
                    // if we're on the last arg, find commands similar to it
                    if (args.length == i + 1)
                    {
                        for (String key : subCommands.keySet())
                        {
                            if (key.startsWith(args[i]))
                            {
                                suggestions.add(key);
                            }
                        }

                        return suggestions;
                    }
                    else
                    {
                        // otherwise find subcommands of this arg if any and continue
                        if (subCommands.get(args[i]) != null)
                        {
                            subCommands = subCommands.get(args[i]).getSubCommands();
                        }
                        else
                        {
                            return new ArrayList<>();
                        }
                    }
                }
            }
        }

        return new ArrayList<>();
    }
}
