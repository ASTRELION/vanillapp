package com.astrelion.Vanillapp.controllers;

import org.bukkit.event.Listener;

import java.io.IOException;

public abstract class ARegisterable implements Listener
{
    /**
     * If this object has been registered yet
     */
    protected boolean isRegistered = false;
    protected boolean isSelfRegistered = false;

    public abstract void register();

    /**
     * Controllers reference each other, init essential parts first
     */
    public void registerSelf()
    {
        //
    }
}
