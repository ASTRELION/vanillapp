package com.astrelion.Vanillapp.controllers;

import com.astrelion.Vanillapp.Vanillapp;
import com.astrelion.Vanillapp.commands.SpawnCommand;
import com.astrelion.Vanillapp.commands.SubCommand;
import com.astrelion.Vanillapp.commands.VanillappTabCompleter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class CommandController extends ARegisterable implements CommandExecutor
{
    protected static final String BASE_COMMAND = "vpp";
    protected JavaPlugin plugin;
    protected Vanillapp vanillapp;
    protected VanillappTabCompleter tabCompleter;

    /**
     * A Map of subcommands that branch from this base command
     */
    protected Map<String, SubCommand> subCommands = new HashMap<>();

    /**
     * The Controller and Base Command of Vanilla++
     * @param vanillapp
     */
    public CommandController(Vanillapp vanillapp)
    {
        this.plugin = vanillapp;
        this.vanillapp = vanillapp;

        vanillapp.getCommand(BASE_COMMAND).setExecutor(this);
    }

    public Vanillapp getVanillapp()
    {
        return this.vanillapp;
    }

    public Map<String, SubCommand> getSubCommands()
    {
        return this.subCommands;
    }

    /**
     *
     * @param sender who/what sent the command
     * @param command the BASE command, ex. /vpp (this)
     * @param label the command label, ex. "vpp"
     * @param args the list of subcommands\arguments
     * @return if the command succeeded, false will display usage
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (sender instanceof Player)
        {
            if (args.length > 0)
            {
                SubCommand subCommand = subCommands.get(args[0].toLowerCase());

                if (subCommand != null)
                {
                    String[] newArgs = new String[args.length - 1];
                    // trim the subcommand from arg list to pass new arg list
                    System.arraycopy(args, 1, newArgs, 0, args.length - 1);
                    return subCommand.onCommand(sender, command, newArgs);
                }
            }
            else
            {
                // TODO send a help message
                sender.sendMessage("- Vanilla++ Help -");
                return true;
            }
        }

        return false;
    }

    @Override
    public void registerSelf()
    {
        if (!isSelfRegistered)
        {
            isSelfRegistered = true;

            // subcommands
            subCommands.put("spawn", new SpawnCommand(this));

            // base command and completion
            this.tabCompleter = new VanillappTabCompleter(this);
            this.getVanillapp().getCommand(BASE_COMMAND).setTabCompleter(this.tabCompleter);
        }
    }

    public void register()
    {
        if (!isRegistered)
        {
            isRegistered = true;

            for (SubCommand subCommand : subCommands.values())
            {
                subCommand.register();
            }
        }
    }
}
