package com.astrelion.Vanillapp.controllers;

import com.astrelion.Vanillapp.Vanillapp;
import com.astrelion.Vanillapp.mobs.AMob;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConfigController extends ARegisterable
{
    private final File DATA_DIR;

    private final Vanillapp vanillapp;
    private SpawnController spawnController;

    private Map<String, FileConfiguration> mobConfigs = new HashMap<>();

    public ConfigController(Vanillapp vanillapp)
    {
        this.vanillapp = vanillapp;
        this.DATA_DIR = vanillapp.getDataFolder();
    }

    public Vanillapp getVanillapp()
    {
        return this.vanillapp;
    }

    public FileConfiguration getMobConfig(String mob)
    {
        return mobConfigs.get(mob);
    }

    @Override
    public void registerSelf()
    {
        if (!isSelfRegistered)
        {
            isSelfRegistered = true;

            this.spawnController = vanillapp.getSpawnController();
        }
    }

    private void registerMobConfig(File file)
    {
        Map<String, AMob> mobs = this.spawnController.getMobs();
        String fileName = file.getName().substring(0, file.getName().lastIndexOf(".yml"));
        String mobClass = "empty";
        boolean isCustomMob = true;

        if (mobs.containsKey(fileName))
        {
            mobClass = fileName;
            isCustomMob = false;
        }

        YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

        System.out.println(config.get("data"));
        // insert data if creating a new file
        if (!config.isSet("data"))
        {
            // this will automatically serialize
            config.set("data", mobs.get(mobClass));
        }

        // replace SpawnController mob with serialized object
        AMob newMob = (AMob) config.get("data"); // this will automatically deserialize
        newMob.setSpawnController(this.spawnController);
//        // temp
//        Entity en = newMob.spawnOne(new Location(getVanillapp().getServer().getWorld("world"), 0, 0, 0));
//        if (en instanceof LivingEntity)
//            newMob.mobToConfigHelper(en);
//        // endtemp

        if (isCustomMob)
        {
            spawnController.replaceMob(fileName, newMob);
            mobConfigs.put(fileName, config);
        }
        else
        {
            spawnController.replaceMob(mobClass, newMob);
            mobConfigs.put(mobClass, config);
        }

        // save the file
        try
        {
            config.save(file);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        getVanillapp().getLogger().warning("Initialized " + newMob.getClass().getSimpleName() + ": " + newMob.getName());
    }

    @Override
    public void register()
    {
        if (!isRegistered)
        {
            isRegistered = true;

//            File directory = new File(this.DATA_DIR.toString() + this.MOB_PATH);
//            if (!directory.exists())
//            {
//                directory.mkdir();
//            }

            Map<String, AMob> mobs = this.spawnController.getMobs();
            for (String key : mobs.keySet())
            {
                ConfigurationSerialization.registerClass(mobs.get(key).getClass(), mobs.get(key).getName());
                //registerConfig(new File(this.DATA_DIR, this.MOB_PATH + key + ".yml"));
            }

            final String MOB_PATH = "/mobs/";
            try(Stream<Path> paths = Files.walk(Paths.get(this.DATA_DIR.toString(), MOB_PATH)))
            {
                List<File> files = paths.filter(Files::isRegularFile).map(Path::toFile).collect(Collectors.toList());

                for (File f : files)
                {
                    registerMobConfig(f);
                }
            }
            catch(IOException e)
            {
                getVanillapp().getLogger().severe("Could not read file path.");
            }
        }
    }
}
