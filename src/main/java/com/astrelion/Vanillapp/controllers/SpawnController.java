package com.astrelion.Vanillapp.controllers;

import com.astrelion.Vanillapp.Vanillapp;
import com.astrelion.Vanillapp.mobs.*;
import com.astrelion.Vanillapp.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.Listener;

import java.util.*;

public class SpawnController extends ARegisterable implements Listener
{
    private final Vanillapp vanillapp;
    private final ConfigController configController;
    private final Map<String, AMob> mobs = new HashMap<>();

    public SpawnController(Vanillapp vanillapp)
    {
        this.vanillapp = vanillapp;
        this.configController = vanillapp.getConfigController();
    }

    public Vanillapp getVanillapp()
    {
        return this.vanillapp;
    }

    public Map<String, AMob> getMobs()
    {
        return mobs;
    }

    public AMob getMob(String mobClass)
    {
        return this.mobs.getOrDefault(mobClass, null);
    }

    public void replaceMob(String key, AMob newMob)
    {
        mobs.put(key, newMob);
    }

    @EventHandler
    public void onEntitySpawnEvent(EntitySpawnEvent event)
    {
        Random random = new Random();

        for (AMob mob : getMobs().values())
        {
            Entity entity = event.getEntity();

            if (entity.getCustomName() != null &&
                ChatColor.stripColor(entity.getCustomName()).equalsIgnoreCase(Util.toTitleCase(mob.getName())))
            {
                break;
            }
            else if (mob.getReplacementEntities() != null)
            {
                for (EntityType replacement : mob.getReplacementEntities().keySet())
                {
                    if (event.getEntityType() == replacement && Util.observeEvent(mob.getReplacementEntities().get(replacement)))
                    {
                        event.getEntity().remove();
                        mob.spawnOne(event.getLocation());
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void registerSelf()
    {
        if (!isSelfRegistered)
        {
            isSelfRegistered = true;

            List<AMob> tempMobList = Arrays.asList(
                new Bandit(this),
                new BroodMother(this),
                new ChargedCreeper(this),
                new Empty(null),
                new Ghost(this),
                new GiantMagmaCube(this),
                new GiantSlime(this),
                new GiantZombie(this),
                new Hellhound(this),
                new IllusionerIllager(this),
                new KillerBunny(this),
                new Lich(this),
                new Magmaman(this),
                new MinecartSpawner(this),
                new RabbitToast(this),
                new RainbowSheep(this),
                new SkeletonMelee(this),
                new SkeletonRider(this),
                new Slimeman(this),
                new TestMob(this),
                new VindicatorJohnny(this),
                //new Wraith(this),
                new ZombieRider(this)
            );

            for (AMob m : tempMobList)
            {
                mobs.put(m.getName(), m);
            }
        }
    }

    public void register()
    {
        if (!isRegistered)
        {
            isRegistered = true;
        }
    }
}
