package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import com.astrelion.Vanillapp.util.Util;
import org.bukkit.*;
import org.bukkit.attribute.Attributable;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public abstract class AMob implements ConfigurationSerializable
{
    protected SpawnController spawnController;

    protected String name = "unnamed_mob";
    protected ChatColor nameColor = ChatColor.WHITE;

    /** ATTRIBUTES **/
    // https://papermc.io/javadocs/paper/1.16/org/bukkit/attribute/Attribute.html
    // defaults based off: https://minecraft.gamepedia.com/Attribute
    protected double maxHealth = 20.0;
    protected double armor = 2.0;
    protected double armorToughness = 1.0;
    protected double knockbackResistance = 0.5;
    protected double attackDamage = 4.0;
    protected double attackKnockback = 1.0;
    protected double moveSpeed = 0.2;
    protected double followRange = 32.0;

    /**
     * POTION_EFFECT:
     *   duration: 0
     *   strength: 0
     */
    protected Map<PotionEffectType, Map<String, Integer>> potionEffects = new HashMap<>();

    /**
     * helmet:
     *   item: ITEM_NAME
     *   dropChance: 0.0
     *   enchantments:
     *     ENCHANT: 0
     *   color:
     *   - 0
     *   - 0
     *   - 0
     */
    protected Map<String, MobEquipment> mobEquipmentMap = new HashMap<>(){{
        put("helmet", new MobEquipment());
        put("chestplate", new MobEquipment());
        put("leggings", new MobEquipment());
        put("boots", new MobEquipment());
        put("mainHand", new MobEquipment());
        put("offHand", new MobEquipment());
    }};

    /** ENTITY INFORMATION **/
    protected EntityType baseEntity = EntityType.ZOMBIE;
    protected Map<EntityType, Float> replacementEntities = new HashMap<>();
    protected String passenger = null;

    public AMob(SpawnController spawnController)
    {
        this.spawnController = spawnController;
    }

    public Map<EntityType, Float> getReplacementEntities()
    {
        return this.replacementEntities;
    }

    @Deprecated
    public void mobToConfigHelper(Entity entity)
    {
        if (entity.getCustomName() != null)
        {
            this.name = ChatColor.stripColor(entity.getCustomName()).toLowerCase();
            this.nameColor = Util.findFirstColor(entity.getCustomName());
        }
        else
        {
            this.name = entity.getName();
            this.nameColor = ChatColor.WHITE;
        }

        this.baseEntity = entity.getType();

        if (entity instanceof LivingEntity)
        {
            LivingEntity livingEntity = (LivingEntity) entity;

            EntityEquipment equipment = livingEntity.getEquipment();

            if (equipment != null)
            {
                mobEquipmentMap.replace("helmet", new MobEquipment(equipment.getHelmet(), equipment.getHelmetDropChance()));
                mobEquipmentMap.replace("chestplate", new MobEquipment(equipment.getChestplate(), equipment.getChestplateDropChance()));
                mobEquipmentMap.replace("leggings", new MobEquipment(equipment.getLeggings(), equipment.getLeggingsDropChance()));
                mobEquipmentMap.replace("boots", new MobEquipment(equipment.getBoots(), equipment.getBootsDropChance()));
                mobEquipmentMap.replace("mainHand", new MobEquipment(equipment.getItemInMainHand(), equipment.getItemInMainHandDropChance()));
                mobEquipmentMap.replace("offHand", new MobEquipment(equipment.getItemInOffHand(), equipment.getItemInOffHandDropChance()));
            }

            for (PotionEffectType p : PotionEffectType.values())
            {
                this.potionEffects.put(p, new HashMap<>(){{
                    put("duration", 0);
                    put("strength", 0);
                }});
            }

            for (PotionEffect p : livingEntity.getActivePotionEffects())
            {
                this.potionEffects.replace(p.getType(), new HashMap<>(){{
                    put("duration", p.getDuration());
                    put("strength", p.getAmplifier());
                }});
            }

            this.maxHealth = livingEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
            this.armor = livingEntity.getAttribute(Attribute.GENERIC_ARMOR).getBaseValue();
            this.armorToughness = livingEntity.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).getBaseValue();
            this.followRange = livingEntity.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).getBaseValue();
            this.moveSpeed = livingEntity.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).getBaseValue();

            if (!(livingEntity instanceof Animals))
            {
                this.attackDamage = livingEntity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).getBaseValue();
                this.attackKnockback = livingEntity.getAttribute(Attribute.GENERIC_ATTACK_KNOCKBACK).getBaseValue();
            }
        }
    }

    @NotNull
    @Override
    public Map<String, Object> serialize()
    {
        Map<String, Object> map = new LinkedHashMap<>(){{
            // entity info
            put("customName", name);
            put("customNameColor", String.valueOf(nameColor.getChar()));
            put("baseEntity", baseEntity.name());
            put("passenger", passenger);
            // attributes
            put("armor", armor);
            put("armorToughness", armorToughness);
            put("attackDamage", attackDamage);
            put("attackKnockback", attackKnockback);
            put("followRange", followRange);
            put("knockbackResistance", knockbackResistance);
            put("maxHealth", maxHealth);
            put("moveSpeed", moveSpeed);
        }};

        map.put("replacementEntities", new LinkedHashMap<>(){{
            for (EntityType key : replacementEntities.keySet())
            {
                put(key.name(), replacementEntities.get(key));
            }
        }});

        // equipment
        map.put("equipment", new LinkedHashMap<>(){{
            for (String key : mobEquipmentMap.keySet())
            {
                MobEquipment equipment = mobEquipmentMap.get(key);
                put(key, equipment.toMap(key));
            }
        }});

        // potion effects
        map.put("potionEffects", new LinkedHashMap<>(){{
            for (PotionEffectType p : potionEffects.keySet())
            {
                put(p.getName(), new LinkedHashMap<>(){{
                    put("duration", potionEffects.get(p).getOrDefault("duration", 0));
                    put("strength", potionEffects.get(p).getOrDefault("strength", 0));
                }});
            }
        }});

        return map;
    }

    // public static <mob_type> deserialize(Map<String, Object> map);
    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new Empty(null));
    }

    /**
     *
     * @param map Map obtained from config YML file
     * @param mob the new mob to create
     * @return the newly created mob
     */
    @SuppressWarnings("unchecked")
    public static AMob deserializeHelper(Map<String, Object> map, AMob mob)
    {
        // entity info
        mob.name = (String) map.getOrDefault("customName", mob.name);
        mob.nameColor = ChatColor.getByChar((String) map.getOrDefault("customNameColor", mob.nameColor));
        mob.baseEntity = EntityType.valueOf((String) map.getOrDefault("baseEntity", mob.baseEntity));
        mob.passenger = (String) map.getOrDefault("passenger", mob.passenger);

        Map<String, Float> newReplacementEntities = (Map<String, Float>) map.getOrDefault("replacementEntities", mob.replacementEntities);

        mob.replacementEntities.clear();
        for (String key : newReplacementEntities.keySet())
        {
            mob.replacementEntities.put(EntityType.valueOf(key), newReplacementEntities.get(key));
        }

        // attributes
        mob.armor = (double) map.getOrDefault("armor", mob.armor);
        mob.armorToughness = (double) map.getOrDefault("armorToughness", mob.armorToughness);
        mob.attackDamage = (double) map.getOrDefault("attackDamage", mob.attackDamage);
        mob.attackKnockback = (double) map.getOrDefault("attackKnockback", mob.attackKnockback);
        mob.followRange = (double) map.getOrDefault("followRange", mob.followRange);
        mob.knockbackResistance = (double) map.getOrDefault("knockbackResistance", mob.knockbackResistance);
        mob.maxHealth = (double) map.getOrDefault("maxHealth", mob.maxHealth);
        mob.moveSpeed = (double) map.getOrDefault("moveSpeed", mob.moveSpeed);

        // equipment
        Map<String, Map<String, Object>> equipmentMap = (Map<String, Map<String, Object>>) map.get("equipment");
        Map<String, MobEquipment> newMobEquipmentMap = new HashMap<>();
        for (String key : equipmentMap.keySet())
        {
            newMobEquipmentMap.put(key, MobEquipment.fromMap(equipmentMap.get(key)));
        }
        mob.mobEquipmentMap = newMobEquipmentMap;

        // potion effects
        Map<String, Object> effects = (Map<String, Object>) map.getOrDefault("potionEffects", null);

        if (effects != null)
        {
            for (PotionEffectType key : PotionEffectType.values())
            {
                Map<String, Integer> effect = (Map<String, Integer>) effects.getOrDefault(key.getName(), new HashMap<>(){{
                    put("duration", 0);
                    put("strength", 0);
                }});

                mob.potionEffects.put(key, new HashMap<>(){{
                    put("duration", effect.get("duration"));
                    put("strength", effect.get("strength"));
                }});
            }
        }

        return mob;
    }

    public void setSpawnController(SpawnController spawnController)
    {
        this.spawnController = spawnController;
    }

    /**
     * Apply all equipment, attributes, potion effects, and passengers
     * to the given entity.
     * @param entity the Entity to apply all configurations to
     */
    public void apply(Entity entity)
    {
        this.nameMob(entity);

        // because of minecarts, armor stands, etc.
        if (entity instanceof LivingEntity)
        {
            LivingEntity livingEntity = (LivingEntity) entity;
            this.equipEquipment(livingEntity);
            this.setAttributes(livingEntity);
            this.applyPotionEffects(livingEntity);
        }

        if (this.passenger != null &&
            !this.passenger.isEmpty() &&
            this.spawnController.getMobs().containsKey(this.passenger))
        {
            entity.addPassenger(this.spawnController.getMobs().get(this.passenger).spawnOne(entity.getLocation()));
        }
    }

    public void equipEquipment(LivingEntity entity)
    {
        if (entity.getEquipment() != null)
        {
            EntityEquipment entityEquipment = entity.getEquipment();
            entityEquipment.clear();

            MobEquipment helmet = this.mobEquipmentMap.get("helmet");
            entityEquipment.setHelmet(helmet.getItem());
            entityEquipment.setHelmetDropChance((float) helmet.getDropChance());

            MobEquipment chestplate = this.mobEquipmentMap.get("chestplate");
            entityEquipment.setChestplate(chestplate.getItem());
            entityEquipment.setChestplateDropChance((float) chestplate.getDropChance());

            MobEquipment leggings = this.mobEquipmentMap.get("leggings");
            entityEquipment.setLeggings(leggings.getItem());
            entityEquipment.setLeggingsDropChance((float) leggings.getDropChance());

            MobEquipment boots = this.mobEquipmentMap.get("boots");
            entityEquipment.setBoots(boots.getItem());
            entityEquipment.setBootsDropChance((float) boots.getDropChance());

            MobEquipment mainHand = this.mobEquipmentMap.get("mainHand");
            entityEquipment.setItemInMainHand(mainHand.getItem());
            entityEquipment.setItemInMainHandDropChance((float) mainHand.getDropChance());

            MobEquipment offHand = this.mobEquipmentMap.get("offHand");
            entityEquipment.setItemInOffHand(offHand.getItem());
            entityEquipment.setItemInOffHandDropChance((float) offHand.getDropChance());
        }
    }

    public void applyPotionEffects(LivingEntity entity)
    {
        for (PotionEffectType key : potionEffects.keySet())
        {
            Map<String, Integer> map = potionEffects.get(key);
            PotionEffect effect = new PotionEffect(key, map.get("duration"), map.get("strength"));
            entity.addPotionEffect(effect);
        }
    }

    public void setAttributes(Attributable attributable)
    {
        Attribute[] attributes = {
            Attribute.GENERIC_ARMOR,
            Attribute.GENERIC_ARMOR_TOUGHNESS,
            Attribute.GENERIC_ATTACK_DAMAGE,
            Attribute.GENERIC_ATTACK_KNOCKBACK,
            Attribute.GENERIC_FOLLOW_RANGE,
            Attribute.GENERIC_KNOCKBACK_RESISTANCE,
            Attribute.GENERIC_MAX_HEALTH,
            Attribute.GENERIC_MOVEMENT_SPEED
        };

        for (Attribute a : attributes)
        {
            boolean success = setAttribute(attributable, a);
        }
    }

    public boolean setAttribute(Attributable attributable, Attribute attribute)
    {
        AttributeInstance instance = attributable.getAttribute(attribute);

        if (instance == null)
        {
            return false;
        }

        double value = instance.getValue();

        switch (attribute)
        {
            case GENERIC_ARMOR:
                value = armor;
                break;

            case GENERIC_ARMOR_TOUGHNESS:
                value = armorToughness;
                break;

            case GENERIC_ATTACK_DAMAGE:
                value = attackDamage;
                break;

            case GENERIC_ATTACK_KNOCKBACK:
                value = attackKnockback;
                break;

            case GENERIC_FOLLOW_RANGE:
                value = followRange;
                break;

            case GENERIC_KNOCKBACK_RESISTANCE:
                value = knockbackResistance;
                break;

            case GENERIC_MAX_HEALTH:
                value = maxHealth;
                break;

            case GENERIC_MOVEMENT_SPEED:
                value = moveSpeed;
                break;

            default:
                break;
        }

        instance.setBaseValue(value);
        return true;
    }

    /**
     * Spawn `amount` of this mob
     * @param location
     * @param amount
     */
    public void spawnMany(Location location, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            spawnOne(location);
        }
    }

    /**
     * Spawn a single mob of this type
     * @param location
     * @return Entity
     */
    public abstract Entity spawnOne(Location location);

    public Material[] getWeapons()
    {
        return new Material[0];
    }

    /**
     * Randomly gets a weapon from valid weapons and returns it
     * @return a random weapon from `getWeapons()`
     */
    public Material getWeapon()
    {
        Random random = new Random();
        Material[] weapons = getWeapons();
        return weapons[random.nextInt(weapons.length)];
    }

    public void nameMob(Entity entity)
    {
        entity.setCustomName(this.nameColor + Util.toTitleCase(this.name));
        entity.setCustomNameVisible(true);
    }

    /**
     * Name this mob from `name` to Title Case
     * @param entity
     * @param name
     */
    public void nameMob(Entity entity, String name)
    {
        entity.setCustomName(this.nameColor + Util.toTitleCase(name));
        entity.setCustomNameVisible(true);
    }

    public void nameMob(Entity entity, String name, ChatColor color)
    {
        nameMob(entity, name);
        entity.setCustomName(color + entity.getCustomName());
    }

    public String getName()
    {
        return this.name;
    }

    /**
     * Check if given Entity is an instance of this AMob
     * @param entity the Entity to check
     * @return true if both entities have the same custom name, false otherwise
     */
    public boolean equals(Entity entity)
    {
        if (entity.getCustomName() != null)
        {
            String entityName = ChatColor.stripColor(entity.getCustomName());
            return Util.toTitleCase(entityName).equalsIgnoreCase(Util.toTitleCase(this.getName()));
        }

        return false;
    }
}
