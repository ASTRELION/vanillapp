package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.Map;

@SerializableAs("charged_creeper")
public class ChargedCreeper extends AMob
{
    public ChargedCreeper(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "charged_creeper";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new ChargedCreeper(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Creeper chargedCreeper = (Creeper) location.getWorld().spawnEntity(location, EntityType.CREEPER);

        chargedCreeper.setPowered(true);

        this.nameMob(chargedCreeper);

        return chargedCreeper;
    }
}
