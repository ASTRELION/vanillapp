package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;

import java.util.Map;

/**
 * The most general AMob subclass. Every custom mob is an Empty mob with added attributes.
 */
@SerializableAs("empty")
public class Empty extends AMob
{
    public Empty(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "empty";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new Empty(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Entity entity = location.getWorld().spawnEntity(location, this.baseEntity);
        this.apply(entity);

        return entity;
    }
}
