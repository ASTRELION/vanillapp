package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.MagmaCube;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

@SerializableAs("giant_magma_cube")
public class GiantMagmaCube extends AMob
{
    protected int MAGMA_CUBE_SIZE = 20;

    public GiantMagmaCube(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "giant_magma_cube";
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> serialize()
    {
        Map<String, Object> map = super.serialize();
        map.putIfAbsent("custom", new HashMap<>());
        ((Map<String, Integer>) map.get("custom")).put("magmaCubeSize", this.MAGMA_CUBE_SIZE);

        return map;
    }

    @SuppressWarnings("unchecked")
    public static AMob deserialize(Map<String, Object> map)
    {
        GiantMagmaCube giantMagmaCube = (GiantMagmaCube) deserializeHelper(map, new GiantMagmaCube(null));
        giantMagmaCube.MAGMA_CUBE_SIZE = ((Map<String, Integer>) map.get("custom")).get("magmaCubeSize");
        return giantMagmaCube;
    }

    @Override
    public Entity spawnOne(Location location)
    {
        MagmaCube giantMagmaCube = (MagmaCube) location.getWorld().spawnEntity(location, EntityType.MAGMA_CUBE);
        giantMagmaCube.setSize(MAGMA_CUBE_SIZE);
        this.apply(giantMagmaCube);

        return giantMagmaCube;
    }
}
