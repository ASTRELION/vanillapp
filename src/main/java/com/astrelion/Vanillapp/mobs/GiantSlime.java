package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Slime;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

@SerializableAs("giant_slime")
public class GiantSlime extends AMob
{
    protected int SLIME_SIZE = 20;

    public GiantSlime(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "giant_slime";
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> serialize()
    {
        Map<String, Object> map = super.serialize();
        map.putIfAbsent("custom", new HashMap<>());
        ((Map<String, Integer>) map.get("custom")).put("slimeSize", this.SLIME_SIZE);
        return map;
    }

    @SuppressWarnings("unchecked")
    public static AMob deserialize(Map<String, Object> map)
    {
        GiantSlime giantSlime = (GiantSlime) deserializeHelper(map, new GiantSlime(null));
        giantSlime.SLIME_SIZE = ((Map<String, Integer>) map.get("custom")).get("slimeSize");
        return giantSlime;
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Slime giantSlime = (Slime) location.getWorld().spawnEntity(location, EntityType.SLIME);
        giantSlime.setSize(SLIME_SIZE);
        this.apply(giantSlime);

        return giantSlime;
    }
}
