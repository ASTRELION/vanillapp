package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Giant;

import java.util.Map;

@SerializableAs("giant_zombie")
public class GiantZombie extends AMob
{
    public GiantZombie(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "giant";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new GiantZombie(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Giant giant = (Giant) location.getWorld().spawnEntity(location, EntityType.GIANT);

        nameMob(giant);

        return giant;
    }
}
