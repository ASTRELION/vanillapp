package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Wolf;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SerializableAs("hellhound")
public class Hellhound extends AMob
{
    public Hellhound(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "hellhound";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new Hellhound(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Wolf hellhound = (Wolf) location.getWorld().spawnEntity(location, EntityType.WOLF);

        List<PotionEffect> effects = new ArrayList<>(Arrays.asList(
            new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1),
            new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 1)
        ));
        hellhound.addPotionEffects(effects);

        hellhound.setBreed(false);
        hellhound.setCollarColor(DyeColor.BLACK);
        hellhound.setAngry(true);
        hellhound.setFireTicks(Integer.MAX_VALUE);

        this.nameMob(hellhound);

        return hellhound;
    }
}
