package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Illusioner;

import java.util.Map;

@SerializableAs("illusioner")
public class IllusionerIllager extends AMob
{
    public IllusionerIllager(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "illusioner";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new IllusionerIllager(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Illusioner illusioner = (Illusioner) location.getWorld().spawnEntity(location, EntityType.ILLUSIONER);

        this.nameMob(illusioner);

        return illusioner;
    }
}
