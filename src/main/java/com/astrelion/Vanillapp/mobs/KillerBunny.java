package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Rabbit;

import java.util.Map;

@SerializableAs("killer_bunny")
public class KillerBunny extends AMob
{
    public KillerBunny(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "killer_bunny";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new KillerBunny(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Rabbit killerBunny = (Rabbit) location.getWorld().spawnEntity(location, EntityType.RABBIT);

        killerBunny.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);

        this.nameMob(killerBunny);

        return killerBunny;
    }
}
