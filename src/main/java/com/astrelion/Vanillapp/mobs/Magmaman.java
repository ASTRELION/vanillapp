package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.MagmaCube;

import java.util.Map;

@SerializableAs("magmaman")
public class Magmaman extends AMob
{
    public Magmaman(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "magmaman";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new Magmaman(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        MagmaCube magma1 = (MagmaCube) location.getWorld().spawnEntity(location, EntityType.MAGMA_CUBE);
        MagmaCube magma2 = (MagmaCube) location.getWorld().spawnEntity(location, EntityType.MAGMA_CUBE);
        MagmaCube magma3 = (MagmaCube) location.getWorld().spawnEntity(location, EntityType.MAGMA_CUBE);

        magma1.setSize(3);
        magma2.setSize(2);
        magma3.setSize(1);

        magma1.addPassenger(magma2);
        magma2.addPassenger(magma3);

        nameMob(magma1);

        return magma1;
    }
}
