package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.minecart.SpawnerMinecart;

import java.util.Map;

@SerializableAs("spawner_minecart")
public class MinecartSpawner extends AMob
{
    public MinecartSpawner(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "spawner_minecart";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new MinecartSpawner(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        SpawnerMinecart minecartSpawner = (SpawnerMinecart) location.getWorld().spawnEntity(location, EntityType.MINECART_MOB_SPAWNER);

        this.nameMob(minecartSpawner);

        return minecartSpawner;
    }
}
