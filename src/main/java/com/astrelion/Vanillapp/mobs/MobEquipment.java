package com.astrelion.Vanillapp.mobs;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.*;

public class MobEquipment
{
    private ItemStack item;
    private double dropChance;

    public MobEquipment()
    {
        this.item = null;
        this.dropChance = 0.0;
    }

    public MobEquipment(ItemStack item, double dropchance)
    {
        this.item = item;
        this.dropChance = dropchance;
    }

    public Map<String, Object> toMap(String itemName)
    {
        Map<String, Object> itemMap = new LinkedHashMap<>(){{
            put("item", item != null ? item.getType().toString() : null);
            put("dropChance", dropChance);
            put("enchantments", new HashMap<>());
            put("color", null);
        }};

        if (item != null)
        {
            if (item.getItemMeta() instanceof LeatherArmorMeta)
            {
                LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
                itemMap.replace("color", Arrays.asList(
                    meta.getColor().getRed(),
                    meta.getColor().getGreen(),
                    meta.getColor().getBlue()
                ));
            }

            Map<String, Integer> enchantmentMap = new LinkedHashMap<>();
            for (Enchantment enchant : item.getEnchantments().keySet())
            {
                enchantmentMap.put(enchant.getKey().getKey(), item.getEnchantmentLevel(enchant));
            }

            itemMap.replace("enchantments", enchantmentMap);
        }

        return itemMap;
    }

    public static MobEquipment fromMap(Map<String, Object> map)
    {
        String itemName = (String) map.get("item");

        if (itemName != null)
        {
            Material material = Material.getMaterial(itemName);

            if (material != null)
            {
                Map<String, Integer> enchantments = (Map<String, Integer>) map.getOrDefault("enchantments", new HashMap<>());

                double dropChance = (double) map.getOrDefault("dropChance", 0.0);
                ItemStack itemStack = new ItemStack(material);

                if (itemStack.getItemMeta() instanceof LeatherArmorMeta)
                {
                    List<Integer> color = (List<Integer>) map.getOrDefault("color", null);

                    if (color != null)
                    {
                        LeatherArmorMeta armorMeta = (LeatherArmorMeta) itemStack.getItemMeta();
                        armorMeta.setColor(Color.fromRGB(color.get(0), color.get(1), color.get(2)));
                        itemStack.setItemMeta(armorMeta);
                    }
                }

                for (String k : enchantments.keySet())
                {
                    itemStack.addUnsafeEnchantment(Enchantment.getByKey(NamespacedKey.minecraft(k)), enchantments.get(k));
                }

                return new MobEquipment(itemStack, dropChance);
            }
        }

        return new MobEquipment();
    }

    public ItemStack getItem()
    {
        return item;
    }

    public void setItem(ItemStack item)
    {
        this.item = item;
    }

    public double getDropChance()
    {
        return dropChance;
    }

    public void setDropChance(double dropChance)
    {
        this.dropChance = dropChance;
    }
}
