package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Rabbit;

import java.util.Map;

@SerializableAs("rabbit_toast")
public class RabbitToast extends AMob
{
    public RabbitToast(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "toast";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new RabbitToast(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Rabbit toast = (Rabbit) location.getWorld().spawnEntity(location, EntityType.RABBIT);

        this.nameMob(toast, "Toast");

        return toast;
    }
}
