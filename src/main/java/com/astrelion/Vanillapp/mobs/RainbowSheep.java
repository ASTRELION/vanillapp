package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Sheep;

import java.util.Map;

@SerializableAs("rainbow_sheep")
public class RainbowSheep extends AMob
{
    public RainbowSheep(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "rainbow_sheep";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new RainbowSheep(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Sheep rainbowSheep = (Sheep) location.getWorld().spawnEntity(location, EntityType.SHEEP);

        this.nameMob(rainbowSheep, "jeb_");

        return rainbowSheep;
    }

    @Override
    public void nameMob(Entity entity, String string)
    {
        entity.setCustomName(string);
        entity.setCustomNameVisible(true);
    }
}
