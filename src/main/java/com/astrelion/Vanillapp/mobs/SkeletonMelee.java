package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

@SerializableAs("melee_skeleton")
public class SkeletonMelee extends AMob
{
    public SkeletonMelee(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "melee_skeleton";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new SkeletonMelee(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Skeleton skeleton = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);
        skeleton.getEquipment().setItemInMainHand(new ItemStack(getWeapon()));

        nameMob(skeleton);

        return skeleton;
    }

    @Override
    public Material[] getWeapons()
    {
        return new Material[] {
            Material.STONE_SWORD,
            Material.STONE_AXE
        };
    }
}
