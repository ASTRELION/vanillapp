package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Map;

@SerializableAs("skeleton_rider")
public class SkeletonRider extends AMob
{
    public SkeletonRider(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "skeleton_rider";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new SkeletonRider(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        SkeletonHorse skeletonHorse = (SkeletonHorse) location.getWorld().spawnEntity(location, EntityType.SKELETON_HORSE);
        Skeleton skeleton = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);

        skeletonHorse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
        skeleton.getEquipment().setItemInMainHand(new ItemStack(Material.BOW));
        skeleton.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 1));

        skeletonHorse.setTamed(true);
        skeletonHorse.addPassenger(skeleton);

        this.nameMob(skeleton);

        return skeleton;
    }
}
