package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Slime;

import java.util.Map;

@SerializableAs("slimeman")
public class Slimeman extends AMob
{
    public Slimeman(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "slimeman";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new Slimeman(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Slime slime1 = (Slime) location.getWorld().spawnEntity(location, EntityType.SLIME);
        Slime slime2 = (Slime) location.getWorld().spawnEntity(location, EntityType.SLIME);
        Slime slime3 = (Slime) location.getWorld().spawnEntity(location, EntityType.SLIME);

        slime1.setSize(3);
        slime2.setSize(2);
        slime3.setSize(1);

        slime1.addPassenger(slime2);
        slime2.addPassenger(slime3);

        nameMob(slime1);

        return slime1;
    }
}
