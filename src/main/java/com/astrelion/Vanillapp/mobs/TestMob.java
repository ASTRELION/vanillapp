package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

@SerializableAs("test")
public class TestMob extends AMob
{
    public TestMob(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "test_mob";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new TestMob(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        //Fluid water = Fluid.WATER;
        Material water = Material.WATER;
        Skeleton skeleton = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);
        skeleton.getEquipment().setHelmet(new ItemStack(Material.LAVA));
        skeleton.getEquipment().setChestplate(new ItemStack(Material.WATER));
        skeleton.getEquipment().setLeggings(new ItemStack(Material.WATER));
        skeleton.getEquipment().setBoots(new ItemStack(Material.WATER));
        //location.getWorld().spawnFallingBlock(location, water);

        return skeleton;
    }
}
