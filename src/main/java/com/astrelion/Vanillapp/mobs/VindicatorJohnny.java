package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Vindicator;

import java.util.Map;

@SerializableAs("johnny")
public class VindicatorJohnny extends AMob
{
    public VindicatorJohnny(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "johnny";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new VindicatorJohnny(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        Vindicator johnny = (Vindicator) location.getWorld().spawnEntity(location, EntityType.VINDICATOR);

        this.nameMob(johnny);

        return johnny;
    }
}
