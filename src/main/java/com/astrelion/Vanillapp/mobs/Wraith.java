package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.*;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

@SerializableAs("wraith")
public class Wraith extends AMob
{
    public Wraith(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "wraith";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new Wraith(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);

        LeatherArmorMeta chestMeta = (LeatherArmorMeta) chest.getItemMeta();
        chestMeta.setColor(Color.fromRGB(50, 50, 50));
        chest.setItemMeta(chestMeta);

        Zombie wraith = (Zombie) location.getWorld().spawnEntity(location, EntityType.ZOMBIE);
        wraith.getEquipment().clear();

        wraith.getEquipment().setHelmet(new ItemStack(Material.WITHER_SKELETON_SKULL));
        wraith.getEquipment().setHelmetDropChance(0);
        wraith.getEquipment().setChestplate(chest);

        ItemStack weapon = new ItemStack(Material.IRON_HOE);
        wraith.getEquipment().setItemInMainHand(weapon);

        List<PotionEffect> effects = new ArrayList<>(Arrays.asList(
            new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1),
            new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.MAX_VALUE, 1),
            new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1),
            new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2),
            new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 1),
            new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 1)
        ));
        wraith.addPotionEffects(effects);

        wraith.setCanPickupItems(false);
        wraith.setShouldBurnInDay(false);

//        setAttributes(wraith);
//        applyPotionEffects(wraith);
        nameMob(wraith);

        return wraith;
    }
}
