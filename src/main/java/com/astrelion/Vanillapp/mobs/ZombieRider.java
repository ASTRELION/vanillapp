package com.astrelion.Vanillapp.mobs;

import com.astrelion.Vanillapp.controllers.SpawnController;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

@SerializableAs("zombie_rider")
public class ZombieRider extends AMob
{
    public ZombieRider(SpawnController spawnController)
    {
        super(spawnController);
        this.name = "zombie_rider";
    }

    public static AMob deserialize(Map<String, Object> map)
    {
        return deserializeHelper(map, new ZombieRider(null));
    }

    @Override
    public Entity spawnOne(Location location)
    {
        ZombieHorse zombieHorse = (ZombieHorse) location.getWorld().spawnEntity(location, EntityType.ZOMBIE_HORSE);
        Zombie zombie = (Zombie) location.getWorld().spawnEntity(location, EntityType.ZOMBIE);

        zombieHorse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
        zombie.getEquipment().setItemInMainHand(new ItemStack(this.getWeapon()));

        zombieHorse.setTamed(true);
        zombieHorse.addPassenger(zombie);

        zombie.setShouldBurnInDay(false);

        this.nameMob(zombie);

        return zombie;
    }

    @Override
    public Material[] getWeapons()
    {
        return new Material[] {
                Material.STONE_SWORD,
                Material.STONE_AXE
        };
    }
}
